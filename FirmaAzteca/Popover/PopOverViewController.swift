//
//  PopOverViewController.swift
//  FirmaAzteca
//
//  Created by Gibbs on 20/04/16.
//  Copyright © 2016 Gibbs. All rights reserved.
//

import Foundation
import UIKit

protocol PopupNavigationDelegate {
    func popupSelectionMade(segue: String)
}


class PopOverViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        let frameSize: CGPoint = CGPointMake(UIScreen.mainScreen().bounds.size.width*0.5, UIScreen.mainScreen().bounds.size.height*0.5)
//        self.preferredContentSize = CGSizeMake(frameSize.x,frameSize.y);
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func dismissButtonPressed(sender: AnyObject?) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
}