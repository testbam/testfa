//
//  ViewController.swift
//  FirmaAzteca
//
//  Created by Gibbs on 19/04/16.
//  Copyright © 2016 Gibbs. All rights reserved.
//

import UIKit

enum Segue: String {
    case popoverSegue
}

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate {


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 12.0/255.0, green: 79.0/255.0, blue: 50.0/255.0, alpha: 1.0);
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()];

        //--- LOGO --- 
//        let logo = UIImage(named: "bgPlecaTop2");
//        let imageView = UIImageView(image: logo)
//        self.navigationItem.titleView = imageView;
    }

    // MARK: - popover
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "popoverSegue" {
            let popoverViewController = segue.destinationViewController
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
            popoverViewController.popoverPresentationController!.delegate = self
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    @IBAction func Ingresa(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let login = storyboard.instantiateViewControllerWithIdentifier("LoginVC")
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func asActive(sender: AnyObject) { //FAQVC
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let asactive = storyboard.instantiateViewControllerWithIdentifier("FAQVC")
        self.navigationController?.pushViewController(asactive, animated: true)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

