//
//  FALoginViewController.swift
//  FirmaAzteca
//
//  Created by Gibbs on 20/04/16.
//  Copyright © 2016 Gibbs. All rights reserved.
//

import Foundation
import UIKit

class FALoginViewController: UIViewController {
    
    
    @IBOutlet weak var addCode: UITextField!
    
    @IBOutlet weak var confirmCode: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func asActive(sender: AnyObject) { //FAQVC
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let asactive = storyboard.instantiateViewControllerWithIdentifier("TokenGVC")
        self.navigationController?.pushViewController(asactive, animated: true)
        
        
    }
}